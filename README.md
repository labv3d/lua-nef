# lua-nef

lua library for CSG based on [CGAL](https://www.cgal.org/) and [nef-polyhedron](https://doc.cgal.org/latest/Nef_3/index.html)

This library provides a "nef-polyhedron" class with appropriate CSG operations to build 3D models.
The models can be exported to STL format, or simple surfaces made of triangles.

## 3D Primitives:

* polyhedron
* cube
* sphere
* cylinder
* null
* load
* import

## 2D primitives

* square

## Unary Operations:
* dup
* regularize
* transform

## CSG operations:

* union
* intersection
* difference
* hull
* minkowski sum

## Import/Export:

* load
* import
* save
* export

## Computed information:

* info
* volume
* bbox
* poly

## Overloaded lua operators:
* a+b : union
* a-b : difference
* a*b : intersection

# Example code

![screenshot1](img/screenshot1.png)

```lua
nef=require("nef")
a=nef.cube{x=1.5,y=1.5,z=1.5,center=true}
b=nef.sphere{r=1,n=64}
c=a-b
c:export{name="out.stl"}
```

# Building and Dependencies

### On Linux (Mint/Ubuntu/Debian)

sudo apt-get install libcgal-dev libcgal-qt5-dev libeigen3-dev

### On Linux (Arch)

sudo pacman -S boost eigen cgal

### Windows and Mac

(did not check)


# TODO

* implement caching
* add 2D primitives, as well as extrusion and projection operations
* add minkowski operation
* provide volumetric triangulation into convex parts
* add a position/orientation system on top of transform()

