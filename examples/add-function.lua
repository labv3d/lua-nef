--
-- how to add new functions
-- (constructors or static functions)
--

nef=require("nef")

for k,v in pairs(nef) do print(k,v) end

--
-- a cylinder with two provided end points a and b
-- {ax,ay,az,bx,by,bz,r}
--
function nef.tube(p)
		local ax,ay,az=p.ax or 0,p.ay or 0,p.az or 0
		local bx,by,bz=p.bx or 0,p.by or 0,p.bz or 1
		local dx,dy,dz=bx-ax,by-ay,bz-az
		local n=math.sqrt(dx*dx+dy*dy+dz*dz)
		if n==0 then
				return nef.null{}
		end
		local b=dx*dx+dy*dy
		local a11=(dy*dy + (dx*dx*dz)/n)/b
		local a12=(dx*dy*(-1 + dz/n))/b
		local a13=dx/n
		local a21=(dx*dy*(-1 + dz/n))/b
		local a22=(dx*dx + (dy*dy*dz)/n)/b
		local a23=dy/n
		local a31=-dx/n
		local a32=-dy/n
		local a33=dz/n
		local cyl=nef.cylinder{h=n,r1=p.r or 1,r2=p.r or 1,n=32}
		-- move base to origin
		cyl:transform{1,0,0,0, 0,1,0,0, 0,0,1,n/2}
		-- align with a-b
		cyl:transform{a11,a12,a13,0, a21,a22,a23,0, a31,a32,a33,0}
		-- move base to a
		cyl:transform{1,0,0,ax, 0,1,0,ay, 0,0,1,az}
		local bouta=nef.sphere{r=p.r or 1,n=32}
		bouta:transform{1,0,0,ax, 0,1,0,ay, 0,0,1,az}
		local boutb=nef.sphere{r=p.r or 1,n=32}
		boutb:transform{1,0,0,bx, 0,1,0,by, 0,0,1,bz}
		return cyl+bouta+boutb
end

q=nef.cube{}
a=nef.tube{ax=1,ay=1,az=1,bx=3,by=3,bz=3,r=0.25}
b=nef.tube{ax=3,ay=3,az=3,bx=0,by=-3,bz=2,r=0.25}
c=nef.tube{ax=0,ay=-3,az=2,bx=0,by=0,bz=0,r=0.25}
fin=a+b+c+q
fin:export{name="out.stl"}
--fin:save{name="out.nef"}



