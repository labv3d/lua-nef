--
-- how to add new methods
--

nef=require("nef")

-- get the object metatable
m=getmetatable(nef.null{})

for k,v in pairs(m) do print(k,v) end

--
-- simple transformations
--
-- note: each transform is applied to the nef vertices.
-- they should be accumulated in a single transform
-- and be handled at another level
-- but this is for demonstration only
--

function m:identity(p) self:transform{1,0,0,0, 0,1,0,0, 0,0,1,0} end
function m:translate(p) self:transform{
		1,0,0,p.x or 0,
		0,1,0,p.y or 0,
		0,0,1,p.z or 0} end
function m:rotateX(p)
		local c,s=math.cos(p.angle or 0),math.sin(p.angle or 0)
		self:transform{
				1,0,0,0,
				0,c,-s,0,
				0,s,c,0
		}
end
function m:rotateY(p)
		local c,s=math.cos(p.angle or 0),math.sin(p.angle or 0)
		self:transform{
				c,0,-s,0,
				0,1,0,0,
				s,0,c,0
		}
end
function m:rotateZ(p)
		local c,s=math.cos(p.angle or 0),math.sin(p.angle or 0)
		self:transform{
				c,-s,0,0,
				s,c,0,0,
				0,0,1,0
		}
end

-- {ax,ay,ax,cx,cy,cz,angle} axis=ax,ay,az, center=cx,cy,cz
function m:rotate(p)
	local c,s=math.cos(p.angle or 0),math.sin(p.angle or 0)
	local ax,ay,az=p.ax or 0,p.ay or 0,p.az or 0
	local b=ax*ax+ay*ay+az*az
	if b==0 then ax=1;b=1 end -- no axis = x axis
	local bb=math.sqrt(b)
	local d=ax*ax+ay*ay
	local a11=(ax*ax*d + (ax*ax*az*az + ay*ay*b)*c)/(b*d)
	local a22=(ay*ay*d + (ay*ay*az*az + ax*ax*b)*c)/(b*d)
	local a33=(az*az*d + d*d*c)/(b*d)
	local a12=(ax*ay*(d+(az*az-b)*c) - az*bb*d*s)/(b*d)
	local a21=(ax*ay*(d+(az*az-b)*c) + az*bb*d*s)/(b*d)
	local a13=(-ax*az*d*(c-1) + ay*bb*d*s)/(b*d)
	local a23=(-ay*az*d*(c-1) - ax*bb*d*s)/(b*d)
	local a31=(-ax*az*d*(c-1) - ay*bb*d*s)/(b*d)
	local a32=(-ay*az*d*(c-1) + ax*bb*d*s)/(b*d)
	-- center of rotation
	local cx,cy,cz=p.cx or 0, p.cy or 0, p.cz or 0
	local a14=(1-a11)*cx -     a12*cy -     a13*cz
	local a24=   -a21*cx + (1-a22)*cy -     a23*cz
	local a34=   -a31*cx -     a32*cy + (1-a33)*cz
	self:transform{a11,a12,a13,a14, a21,a22,a23,a24, a31,a32,a33,a34}
end

for k,v in pairs(m) do print(k,v) end

a=nef.cube{center=true}
--b:translate{x=1.2}
--b:rotateZ{angle=math.pi/4}
c={}
for g=0,359,10 do
		b=a:dup{}
		b:rotate{angle=g*math.pi/180,ax=1,ay=1,az=1,cx=1,cy=-1,cz=1}
		--t=b:poly{}
		--bb=nef.polyhedron(t)
		table.insert(c,b)
end
d=nef.union(c)
d:export{name="out.stl"}


