
Inspired by OpenScad, this library provides access to the CGAL library for
constructive solid geometry (CSG) based on exact representations and operations.

It is intented to closely follow CGAL, so there is no high level modeling support or
preview, and no visualisation.

For reference:
	- [OpenSCad](https://openscad.org)
	- [CGAL](https://cgal.org)
	- [Nef-polyhedron](https://doc.cgal.org/latest/Nef_3/index.html)


[[_TOC_]]

## class **nef**

The library is a simple lua module, used as a class.

```lua
nef=require("nef")
```

### 3D Primitives constructors

#### **polyhedron { points={x1,y1,z1,x2,y2,z2,...}, faces={a1,b1,c1,a2,b2,c2,...} }**

  Creates a nef from the list of triangles provided by a list off *faces* and a list of 3d *points*.
  The first triangle has indexes (*a1*,*b1*,*c1*) into the list of points, the second triangle has indexes (*a2*,*b2*,*c2*) and so on.
  Notice the lists *points* and *faces* are one dimensional.
  -> Make sure your the surface form a solid
  -> follow lua tradition, indexes start at 1, not 0.

  Exemple code for a tetrahedron:
  ```lua
  nef=require("nef")
  p=nef.polyhedronp={points={0., 0., 0.612372, -0.288675, -0.5, -0.204124, -0.288675, 0.5, -0.204124, 0.57735, 0., -0.204124} , faces={2, 3, 4, 3, 2, 1, 4, 1, 2, 1, 4, 3}}
  p:export{name="out.stl"}

  ```

#### **cube { x=1.0, y=1.0, z=1.0, center=false }**

  Creates a cube with size of size *x*,*y* and *z* along each respective axis.
  If *center* is true, then the cube will be centered around (0,0,0). Otherwise, its lower corner will be (0,0,0) and highest corner (x,y,z).

  Example code:
  ```lua
  nef=require("nef")
  c=nef.cube{center=false}
  d=nef.cube{x=0.25,y=0.25,z=2,center=true}
  (c+d):export{name="out.stl"}

  ```

#### **sphere { r=1.0, n=8, triangles=0 }**
  Create a sphere with radius **r**, using *n* points to cover the equator, and near *n/2* points between the poles.
  If triangles is true, then the sphere will be formed by triangles, otherwise it will be composed of quads.

  Example code:
  ```lua
  nef=require("nef")
  a=nef.sphere{r=1,n=32,triangles=0}
  b=nef.sphere{r=1,n=32,triangles=1}
  b:transform{1,0,0,2,0,1,0,0,0,0,1,0}
  (a+b):export{name="out.stl"}
  ```
  ![spheres](../img/example-spheres.png)

#### **cylinder { h=1.0, r1=1.0, r2=1.0, n=8, triangles=0 }**
  Create a cylinder of height *h*, with radius *r1* at top and *r2* at base.
  The number of points around the cylinder is *n*, and *triangles* determines if facets are triangles or quads.
  -> cylinders are always centered
  -> if a single radius is provided (either *r1* or *r2*), they are both set to that same value.

  Examples code:
  ```lua
  nef=require("nef")
  a=nef.cylinder{h=2.0,r1=0.5,r2=1.5,n=32,triangles=0}
  b=nef.cylinder{h=1.0,r1=1.0,r2=1.0,n=32,triangles=1}
  b:transform{1,0,0,2,0,1,0,0,0,0,1,0}
  (a+b):export{name="out.stl"}
  ```
  ![spheres](../img/example-cyl.png)

#### **null { }**
  Create an empty nef polyhedron.
  Not very useful, except maybe for debugging....

### I/O constructors

#### **load { name="abc.nef" }**
  Create a nef from a saved nef representation (done with [**save**](#save-nameabcnef-).
  Useful to preserve intermediate nef computations.
  The file format is ascii and described [here](https://doc.cgal.org/latest/Nef_3/index.html#title11).

  Examples code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  a:save{name="abc.nef"}
  b=nef.load{name="abc.nef"}
  b:export{name="out.stl"}
  ```

#### **import { name="model.stl" }**
  Creates a Nef polyhedron from an STL file.
  Many things ca go wrong here... STL does not enforce a valid solid, so the surface can have holes, or all kinds of non-manifold problems.
  -> extremely brittle function. Fails a lot.

  Examples code (not working in this release):
  ```lua
  nef=require("nef")
  a=nef.cube{}
  a:export{name="in.stl"}
  b=nef.import{name="in.stl"}
  b:export{name="out.stl"}
  ```
  

### 2D primitives

At the moment, no 2d primitive is implemented.
This is on the todo list, as well as operations such as extrusions and projections.

### Unary Operations on Nef

#### **dup { }**
  Returns a duplicate of a nef polyhedron.

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=a:dup()
  c=a
  b:transform{2,0,0,0,0,1,0,0,0,0,1,0}
  c:transform{3,0,0,0,0,1,0,0,0,0,1,0}
  -- a is not affected by the transform on b
  -- a is affected by the transform on c
  ```

#### **regularize { }**
  Remove from a nef polyhedron any manifold with a dimension other then 3.
  This will remove single points, lines and planes (dimensions 0,1 and 2).

  This should be performed automatically after most operations that can result is non-manifolds, such as difference. So this function is rarely called by itself.

  Example code (must be verified, out1.nef and out2.nef should differ):
  ```lua
  nef=require("nef")
  a=nef.cube{z=2}
  b=nef.cube{}
  b:transform{1,0,0,1, 0,1,0,0, 0,0,1,0}
  c=(a*b)
  c:save{name="out1.nef"}
  print("before:",c:info{})
  c:regularize()
  c:save{name="out2.nef"}
  print("after:",c:info{})
  ```

#### **transform { a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34 }**
  Applies an affine transform to the 3D points of a nef.
  The matrix is provided flat, as 12 numbers.

  Example code:
  ```lua
  nef=require("nef")
  -- simple transforms
  function translate(n,x,y,z) print(n,x,y,z) n:transform{1,0,0,x, 0,1,0,y, 0,0,1,z} end
  function rotateX(n,a) local c,s=math.cos(a),math.sin(a) n:transform{1,0,0,0,  0,c,-s,0, 0,s,c,0} end
  a=nef.cube{center=true}
  b=nef.cube{center=true}
  translate(b,-1.2,0,0)
  c=nef.cube{center=true}
  rotateX(c,math.pi/4)
  translate(c,1.2,0,0)
  (a+b+c):export{name="out.stl"}
  ```

  ![transform](../img/example-transform.png)


### I/O Operations on Nef


#### **save { name="abc.nef" }**
  Save a nef in a file with a nef representation, which can be read by [**load**](#load-nameabcnef-).
  Useful to preserve intermediate nef computations.
  The file format is ascii and described [here](https://doc.cgal.org/latest/Nef_3/index.html#title11).

  Examples code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  a:save{name="abc.nef"}
  b=nef.load{name="abc.nef"}
  b:export{name="out.stl"}
  ```

#### **export { name="model.stl" }**
  Save a Nef polyhedron as an STL file.

  Examples code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  a:export{name="in.stl"}
  ```


### CSG operations:

#### **union { a,b,c,... }**
  Perform the union of nef polyhedra *a*, *b*, *c*, ...
  -> Union is overloaded in **+** operator, so **c=a+b** is equivalent to **c=nef.union{a,b}**.
  -> operations are done in order, i.e.  c = a+b+c = (a+b)+c

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=64}
  c=nef.union{a,b}               -- same as c=a+b
  c:export{name="out.stl"}
  ```

  ![union](../img/example-union.png)

#### **intersection { a,b,c,... }**
  Perform the intersection of nef polyhedra *a*, *b*, *c*, ...
  -> Intersection is overloaded in **\*** operator, so **c=a\*b** is equivalent to **c=nef.intersection{a,b}**.
  -> operations are done in order, i.e.  c = a\*b\*c = (a\*b)\*c

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=64}
  c=nef.intersection{a,b}        -- same as c=a*b
  c:export{name="out.stl"}
  ```

  ![intersection](../img/example-intersection.png)

#### **difference { a,b,c,... }**
  Perform the difference of nef polyhedra *a* with *b*, *c*, ...
  -> Difference is overloaded in **-** operator, so **c=a-b** is equivalent to **c=nef.difference{a,b}**.
  -> operations are done in order, i.e.  c = a-b-c = (a-b)-c

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=64}
  c=nef.difference{a,b}        -- same as c=a-b
  c:export{name="out.stl"}
  ```

  ![difference](../img/example-difference.png)

#### **hull { a,b,c,... }**
  Returns the convex hull of the nef polyhedra *a*, *b*, *c*, ...
  -> operations are done in order, i.e.  hull{a,b,c} is the same as hull{hull{a,b},c}

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=64}
  c=nef.hull{a,b}
  c:export{name="out.stl"}
  ```

  ![hull](../img/example-hull.png)


#### **minkowski { a,b }**
  Returns the minkowski sum of the nef polyhedra *a* with *b*
  -> slow! Need optimisation with convex decomposition

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.3,n=16}
  c=nef.minkowski{a,b}
  c:export{name="out.stl"}
  ```

  ![minkowski](../img/example-minkowski.png)





### Computed information

  It is possible to get information on a Nef polyhedron. Beware that some computations can take mon CPU time than others...

#### **info { }**

  Returns a few informations about the nef:
   * valid (0/1)  is 1 if the nef is valid
   * simple (0/1)  is 1 if the nef is simple (only 2D surfaces surrounding 3D volumes, no other manifolds)
   * volumes is the number of volumes in this nef (the exterior also counts as a volume)

  Example code:
  ```lua
  nef=require("nef")
  n=nef.null{}
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=64}
  b:transform{1,0,0,2, 0,1,0,0, 0,0,1,0}
  print("null:",n:info{})
  print("A:",a:info{})
  print("A+B:",(a+b):info{})
  ```
  should output
  ```
  null:	1	1	1
  A:	1	1	2
  A+B:	1	1	3
  ```


#### **volume { }**

   Returns the volume and center of mass of a nef.
The computation requires a convex decomposition of the nef, which can be computational expensive.
   -> The center of mass assumes a uniform material density of 1

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.6,n=16}
  c=(a+b)
  vol,cx,cy,cz = c:volume{}
  print("volume is ",vol)
  print("center of mass is ",cx,cy,cz)
  ```
  shoud output
  ```
  volume is 	1.742126737563
  center of mass is 	0.27366045019166	0.27366045016663	0.27349361000226
  ```


#### **bbox { }**

  Return the bounding box of a nef.
  -> fast to compute

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  b=nef.sphere{r=0.5,n=16}
  c=(a+b)
  xmin,xmax,ymin,ymax,zmin,zmax=c:bbox{}
  print("bbox X",xmin,xmax)
  print("bbox Y",ymin,ymax)
  print("bbox Z",zmin,zmax)
  ```
  shoud output
  ```
  bbox X	-0.5	1.0
  bbox Y	-0.5	1.0
  bbox Z	-0.5	1.0
  ```

#### **poly { }**

  Return the faces and vertices of the nef polyhedron.
  The information is provided in a singe table with two keys: points and faces.
  ```
  { points={x1,y1,z1,x2,y2,z2,...}, faces={a1,b1,c1,a2,b2,c2,...} }
  ```
  The first triangle has indexes (*a1*,*b1*,*c1*) into the list of points, the second triangle has indexes (*a2*,*b2*,*c2*) and so on.
  -> all faces are triangles
  -> indexes start at 1, not zero.
  -> this table is compatible with the [polyhedron](#polyhedron-pointsx1y1z1x2y2z2-facesa1b1c1a2b2c2-) constructor

  Example code:
  ```lua
  nef=require("nef")
  a=nef.cube{}
  t=a:poly{}
  p=t.points
  f=t.faces
  j=1
  for i=1,#p,3 do print("point "..j.." : ",p[i],p[i+1],p[i+2]) j=j+1 end
  for i=1,#f,3 do print("face ",f[i],f[i+1],f[i+2]) end
  ```
  should output
  ```
  point 1 : 	0.0	0.0	1.0
  point 2 : 	1.0	0.0	1.0
  point 3 : 	1.0	1.0	1.0
  point 4 : 	0.0	1.0	1.0
  point 5 : 	0.0	0.0	0.0
  point 6 : 	0.0	1.0	0.0
  point 7 : 	1.0	1.0	0.0
  point 8 : 	1.0	0.0	0.0
  face 	4	2	3
  face 	2	4	1
  face 	5	4	6
  face 	4	5	1
  face 	7	4	3
  face 	4	7	6
  face 	2	7	3
  face 	7	2	8
  face 	5	2	1
  face 	2	5	8
  face 	5	7	8
  face 	7	5	6
  ```

  

### Overloaded lua operators

The basic CSG operations can be performed using the overloaded operators +,-, and \*.
No other overloading is provided.

* a+b : union
* a-b : difference
* a\*b : intersection

### Adding functions and methods

The set of functions and methods are quite basic. Adding functions and methods in lua is quite easy.

#### functions

To add new constructors and static functions, simply add the function to the nef module table directly:

```lua
    nef=require("nef")
    function nef.cappedCylinder(p)
		local h=p.h or 1
		local cyl=nef.cylinder{r1=p.r,r2=p.r,h=h,n=p.n}
		local end1=nef.sphere{r=p.r,n=p.n}
		local end2=nef.sphere{r=p.r,n=p.n}
		end1:transform{1,0,0,0, 0,1,0,0, 0,0,1,-h/2}
		end2:transform{1,0,0,0, 0,1,0,0, 0,0,1,h/2}
		return cyl+end1+end2
    end
    a=nef.cappedCylinder{h=4,r=1.2,n=16}
    a:export{name="out.stl"}
```

![functions](../img/example-functions.png)


#### methods

To add a new methods, which is always called with the nef as a *self* parameter, the metatable of the nef must be modified.

```lua
	nef=require("nef")
	m=getmetatable(nef.null{})
	function m:translate(p) self:transform{
        1,0,0,p.x or 0,
        0,1,0,p.y or 0,
        0,0,1,p.z or 0} end
	a=nef.cube{}
	b=nef.cube{}
	b:translate{x=1,y=1,z=1}
	(a+b):export{name="out.stl"}
```

![functions](../img/example-methods.png)

## More Example code

See the exemples section for some examples:
* [functions](../examples/add-function.lua)
* [methods](../examples/add-method.lua)


