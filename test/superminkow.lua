nef=require("nef")

--
-- Minkowski A X B
--
-- assume que le minkowski est avec un objet B convexe.
--
-- Prend les points et les faces de A
-- Pour chaque face, ona un triangle.
-- On cree un triplet de B aux 3 points, on fait le convex hull
-- Union de tous les convex hull
--
-- si deux objets convexes, alors prendre le plus petit pour
-- generer les triangles....
--

--a=nef.sphere{r=5,n=16}
a1=nef.sphere{r=6,n=16}
a2=nef.sphere{r=6,n=16}
a2:transform{1,0,0,3, 0,1,0,0, 0,0,1,0}
--a=a1-a2
a=a1

b=nef.sphere{n=8,r=1}

--a,b=b,a


t=a:poly{}
p=t.points
f=t.faces

shape={a}

--[[
j=1
for i=1,#p,3 do
		local x,y,z = p[i],p[i+1],p[i+2]
		print(j,":",x,y,z)
		s=b:dup()
		s:transform{1,0,0,x, 0,1,0,y, 0,0,1,z}
		table.insert(shape,s)
		j=j+1
end
--]]


t1=os.clock()
for i=1,#f,3 do
		--if i>5 then break end
		local i1,i2,i3=f[i],f[i+1],f[i+2]
		print(i1,i2,i3)
		local x1,y1,z1=p[i1*3-2],p[i1*3-1],p[i1*3]
		local x2,y2,z2=p[i2*3-2],p[i2*3-1],p[i2*3]
		local x3,y3,z3=p[i3*3-2],p[i3*3-1],p[i3*3]
		s1=b:dup()
		s1:transform{1,0,0,x1, 0,1,0,y1, 0,0,1,z1}
		s2=b:dup()
		s2:transform{1,0,0,x2, 0,1,0,y2, 0,0,1,z2}
		s3=b:dup()
		s3:transform{1,0,0,x3, 0,1,0,y3, 0,0,1,z3}
		s=nef.hull{s1,s2,s3}
		--s:regularize()
		s1=nil
		s2=nil
		s3=nil
		table.insert(shape,s)
end

function over(min1,max1,min2,max2)
		return math.max(max1-min2,0) - math.max(max1-max2,0) -( math.max(min1-min2,0) - math.max(min1-max2,0) )
end

-- overlap de bounding box
function overlap(a,b)
		x1min,x1max,y1min,y1max,z1min,z1max=unpack(a)
		x2min,x2max,y2min,y2max,z2min,z2max=unpack(b)
		--print("1",x1min,x1max)
		--print("2",x2min,x2max)
		xover=over(x1min,x1max,x2min,x2max)
		yover=over(y1min,y1max,y2min,y2max)
		zover=over(z1min,z1max,z2min,z2max)
		--print("over",xover*yover*zover)
		v1=(x1max-x1min)*(y1max-y1min)*(z1max-z1min)
		v2=(x1max-x1min)*(y1max-y1min)*(z1max-z1min)
		return xover*yover*zover/math.min(v1,v2)*100
end

--[[
t2=os.clock()
-- arrange les nef a unioner
bb={}
for i=1,#shape do
		bb[i]={shape[i]:bbox()}
		xmin,xmax,ymin,ymax,zmin,zmax=unpack(bb[i])
		--print(i,"BBOX",xmin,xmax,ymin,ymax,zmin,zmax)
end

while #shape>1 do
		print("----- "..#shape.." shape -----")
		besti=1
		bestj=2
		--besto=overlap({shape[1]:bbox()},{shape[2]:bbox()})
		besto=overlap(bb[1],bb[2])
		for i=1,#shape-1 do
			for j=i+1,#shape do
				--print("testing ",i,"and",j)
				ov=overlap(bb[i],bb[j])
				--print("ov",ov,"besto",besto)
				if ov>besto then
						besti,bestj,besto=i,j,ov
				end
			end
		end
		--print("BEST is",besti,bestj,besto)
		shape[besti]=shape[besti]+shape[bestj]
		bb[besti]={shape[besti]:bbox()}
		table.remove(shape,bestj)
end
t3=os.clock()

final=shape[1]
final:export{name="out.stl"}
--]]

--
t2=os.clock()
print("union de "..#shape.." nef")
final=nef.union(shape)
t3=os.clock()

final:export{name="out.stl"}
--a:export{name="outa.stl"}
--a:export{name="outb.stl"}

print("time hulls = ",t2-t1)
print("time union = ",t3-t2)
print("time total = ",t3-t1)



--[[
t1=os.clock()
c=nef.minkowski{a,b}
t2=os.clock()
print("CPU time is ",t2-t1)
]]--

--a:export{name="outa.stl"}
--b:export{name="outb.stl"}
--c:export{name="outc.stl"}


