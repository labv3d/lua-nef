
nef=require("nef")

a=nef.cube{x=2,y=2,z=2}

b=nef.sphere{r=1.5,n=32}
b:transform{1,0,0,0.5, 0,1,0,0, 0,0,0.5,0}

c=nef.cylinder{n=32}
c:transform{1,0,0,1.5, 0,1,0,0, 0,0,1,0}

d=(b-a)+c
d:export{name="test1.stl"}

d=b-a-c
d:export{name="test2.stl"}

d=(a+c)*b
d:export{name="test3.stl"}

