nef=require("nef")

--[[
a1=nef.cube{x=10,y=10,z=10}
a2=a1:dup()
a2:transform{1,0,0,5, 0,1,0,5, 0,0,1,5}
a=a1-a2
b=nef.sphere{n=16,r=1}
--]]

a1=nef.sphere{r=5,n=7}
a2=nef.cube{x=10,y=10,z=10}
a=a1 -- -a2
b=nef.sphere{n=8,r=1}


t1=os.clock()
c=nef.minkowski{a,b}
t2=os.clock()
print("CPU time is ",t2-t1)

v=a:volume()
print("volume a = ",v)
v=c:volume()
print("volume c = ",v)

a:export{name="outa.stl"}
b:export{name="outb.stl"}
c:export{name="outc.stl"}


